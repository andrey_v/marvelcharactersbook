//
//  DetailViewController.m
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "DetailViewController.h"
#import "CharacterImageCollectionViewCell.h"
#import "CharacterInfoCollectionViewCell.h"
#import "CharacterEntity.h"
#import "AppDelegate.h"

@interface DetailViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) CharacterEntity *character;

@end

@implementation DetailViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureCollectionView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

- (void)dealloc
{
    [_character release];
    [super dealloc];
}

#pragma mark - Configuration

- (void)configureCollectionView
{
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CharacterImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([CharacterImageCollectionViewCell class])];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CharacterInfoCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([CharacterInfoCollectionViewCell class])];
}

- (void)configureWithCharacter:(CharacterEntity *)character
{
    [character retain];
    [_character release];
    
    _character = character;
}

#pragma mark - CollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return [self imageCollectionViewCellForItemAtIndexPath:indexPath];
            
        case 1:
            return [self infoCollectionViewCellForItemAtIndexPath:indexPath];
            
        default:
            NSAssert((indexPath.row > 1), @"Unknown row");
            return nil;
    }
}

- (CharacterImageCollectionViewCell *)imageCollectionViewCellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CharacterImageCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CharacterImageCollectionViewCell class]) forIndexPath:indexPath];
    [cell configureWithCharacter:_character];
    return cell;
}

- (CharacterInfoCollectionViewCell *)infoCollectionViewCellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CharacterInfoCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CharacterInfoCollectionViewCell class]) forIndexPath:indexPath];
    [cell configureWithCharacter:_character];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    switch (indexPath.row) {
        case 0:
            height = [CharacterImageCollectionViewCell defaultHeight];
            break;
            
        case 1:
            height = [CharacterInfoCollectionViewCell heightForCharacter:_character];
            break;
            
        default:
            height = 0;
            break;
    }
    return CGSizeMake([UIApplication sharedApplication].keyWindow.frame.size.width-2*kDefaultSpacing, height);
}

@end






