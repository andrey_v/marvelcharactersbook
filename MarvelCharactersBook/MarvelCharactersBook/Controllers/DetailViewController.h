//
//  DetailViewController.h
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CharacterEntity;

@interface DetailViewController : UICollectionViewController

- (void)configureWithCharacter:(CharacterEntity *)character;

@end
