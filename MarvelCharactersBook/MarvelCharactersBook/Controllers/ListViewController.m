//
//  ListViewController.m
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "ListViewController.h"
#import "CharacterTableViewCell.h"
#import "DetailViewController.h"
#import "CharacterEntity+Model.h"
#import "NetworkService.h"
#import "DataBaseService.h"

@interface ListViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *charactersArray;

@property (nonatomic, strong) NSIndexPath *previousIndexPath;

@end

@implementation ListViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setClearsSelectionOnViewWillAppear:YES];
    [self configureCharactersArray];
}

- (void)dealloc
{   
    [_charactersArray release];
    [_previousIndexPath release];
    [super dealloc];
}

#pragma mark - Configuration

- (void)configureCharactersArray
{
    _charactersArray = [@[] mutableCopy];

    [self loadMore];
}

#pragma mark - TableView DataSource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _charactersArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CharacterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CharacterTableViewCell class]) forIndexPath:indexPath];
    [cell configureWithCharacter:_charactersArray[indexPath.row]];
    
    
    if (_previousIndexPath.row < indexPath.row && indexPath.row == _charactersArray.count-5) {
        [self loadMore];
    }
    [indexPath retain];
    [_previousIndexPath release];
    _previousIndexPath = indexPath;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"DetailViewControllerSeque" sender:_charactersArray[indexPath.row]];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(CharacterEntity *)sender
{
    [(DetailViewController *)segue.destinationViewController configureWithCharacter:sender];
}

#pragma mark - Loading More

- (void)loadMore
{
    [self retain];
    [[NetworkService sharedService] loadCharactersWithOffset:_charactersArray.count success:^(NSArray *entitiesArray) {
        [_charactersArray addObjectsFromArray:entitiesArray];
        [self reloadData:entitiesArray.count];
        [self release];
    } failure:^(NSError *error) {
        if (_charactersArray.count == 0) {
            NSArray *storedArray = [DataBaseService storedCharacterEntitiesArray];
            [_charactersArray addObjectsFromArray:storedArray];
            [self reloadData:storedArray.count];
        }
        [self release];
    }];
}

- (void)reloadData:(NSInteger)addedEntitiesCount
{
    NSMutableArray *indexPathsToInsert = [@[] mutableCopy];
    for (NSInteger i = (_charactersArray.count - addedEntitiesCount); i < _charactersArray.count; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationFade];
    [indexPathsToInsert release];
}

@end
