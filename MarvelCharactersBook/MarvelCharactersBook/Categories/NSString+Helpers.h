//
//  NSString+Helpers.h
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Helpers)

- (CGFloat)heightForDefaultWidth;

- (NSString *)MD5String;

@end
