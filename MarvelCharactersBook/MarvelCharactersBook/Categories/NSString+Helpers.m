//
//  NSString+Helpers.m
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "NSString+Helpers.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Helpers)

- (CGFloat)heightForDefaultWidth
{
    CGSize size = [self boundingRectWithSize:CGSizeMake([UIApplication sharedApplication].keyWindow.frame.size.width - 2*kDefaultSpacing, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]} context:nil].size;
    return size.height;
}

- (NSString *)MD5String
{
    const char			*ptr = [self UTF8String];
    unsigned char		md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(ptr, (CC_LONG) strlen(ptr), md5Buffer);
    
    NSMutableString		*output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", md5Buffer[i]];
    }
    return output;
}

@end
