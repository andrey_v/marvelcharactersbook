//
//  NetworkService.h
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkService : NSObject

+ (instancetype)sharedService;

- (void)loadCharactersWithOffset:(NSInteger)offset success:(void (^)(NSArray *entitiesArray))success failure:(void (^)(NSError *error))failure;

@end
