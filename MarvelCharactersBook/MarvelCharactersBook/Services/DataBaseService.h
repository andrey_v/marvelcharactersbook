//
//  DataBaseService.h
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataBaseService : NSObject

+ (NSArray *)storedCharacterEntitiesArray;
+ (void)storeCharacterEntitiesArray:(NSArray *)array withOffset:(NSInteger)offset;

@end
