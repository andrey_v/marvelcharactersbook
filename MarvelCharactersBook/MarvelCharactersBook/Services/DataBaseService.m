//
//  DataBaseService.m
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "DataBaseService.h"

@implementation DataBaseService

+ (void)storeCharacterEntitiesArray:(NSArray *)array withOffset:(NSInteger)offset
{
    NSMutableArray *arrayToStore = [@[] mutableCopy];
    if (offset > 0) {
        [arrayToStore addObjectsFromArray:[self storedCharacterEntitiesArray]];
    }
    [arrayToStore addObjectsFromArray:array];
    
    [NSKeyedArchiver archiveRootObject:arrayToStore toFile:[DataBaseService filePath]];
}

+ (NSArray *)storedCharacterEntitiesArray
{
    return [NSKeyedUnarchiver unarchiveObjectWithFile:[DataBaseService filePath]];
}

+ (NSString *)filePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"MarvelCharactersDB.txt"];
    return filePath;
}

@end
