//
//  NetworkService.m
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "NetworkService.h"
#import <AFNetworking/AFNetworking.h>
#import "NSString+Helpers.h"
#import "CharacterEntity+Model.h"
#import "DataBaseService.h"

static NSString * const kServerAddress = @"https://gateway.marvel.com/";
static const NSInteger kLimit = 20;
static NSString * const kServerApiKey = @"fe66ed9d24f70fa5e4ce78ec01899f2c";
static NSString * const kServerApiPrivateKey = @"86c5ee461b74712659e1586c75c3629bfb26bced";

@interface NetworkService ()

@property (nonatomic, strong) AFHTTPSessionManager *manager;
@property (nonatomic) BOOL isLoading;

@end

@implementation NetworkService

+ (instancetype)sharedService
{
    static NetworkService *__sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[NetworkService alloc] init];
        [__sharedInstance configureSessionManager];
        __sharedInstance.isLoading = NO;
    });
    return __sharedInstance;
}

- (void)dealloc
{
    [_manager dealloc];
    [super dealloc];
}

- (void)configureSessionManager
{
    _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kServerAddress]];
    _manager.responseSerializer = [AFJSONResponseSerializer serializer];
    _manager.requestSerializer = [AFJSONRequestSerializer serializer];
}

- (void)loadCharactersWithOffset:(NSInteger)offset success:(void (^)(NSArray *entitiesArray))success failure:(void (^)(NSError *error))failure
{
    if (!_isLoading) {
        _isLoading = YES;
        
        NSString *ts = [@([NSDate timeIntervalSinceReferenceDate]) stringValue];
        NSString *hashString = [NSString stringWithFormat:@"%@%@%@", ts, kServerApiPrivateKey, kServerApiKey];
        NSDictionary *parameters = @{@"orderBy":@"name",
                                     @"limit":@(kLimit),
                                     @"offset":@(offset),
                                     @"apikey":kServerApiKey,
                                     @"ts":ts,
                                     @"hash":[hashString MD5String]};
        
        [self requestGET:@"v1/public/characters" parameters:parameters success:^(id responseObject) {
            if (responseObject[@"data"]) {
                NSDictionary *dataDictionary = responseObject[@"data"];
                NSArray *results = dataDictionary[@"results"];
                NSArray *entities = [CharacterEntity entitiesWithArray:results];
                if (entities.count > 0) {
                    [DataBaseService storeCharacterEntitiesArray:entities withOffset:offset];
                }
                if (success) {
                    success(entities);
                }
            }
        } failure:failure];
    }
}

- (NSURLSessionDataTask *)requestGET:(NSString *)request parameters:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self retain];
    return [self.manager GET:request parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
        _isLoading = NO;
        [self release];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
        _isLoading = NO;
        [self release];
    }];
}

@end
