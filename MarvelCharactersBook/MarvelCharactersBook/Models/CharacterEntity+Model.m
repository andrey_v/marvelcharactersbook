//
//  CharacterEntity+Model.m
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "CharacterEntity+Model.h"

@implementation CharacterEntity (Model)

+ (NSArray *)entitiesWithArray:(NSArray *)array
{
    NSMutableArray *entitiesArray = [@[] mutableCopy];
    
    for (NSDictionary *dictionary in array) {
        [entitiesArray addObject:[CharacterEntity entityWithDictonary:dictionary]];
    }
    
    return entitiesArray;
}

+ (instancetype)entityWithDictonary:(NSDictionary *)dictionary
{
    CharacterEntity *entity = [[[CharacterEntity alloc] init] autorelease];
    entity.characterID = dictionary[@"id"];
    entity.name = dictionary[@"name"];
    entity.characterDescription = dictionary[@"description"];
    NSString *path = [dictionary[@"thumbnail"] objectForKey:@"path"];
    NSString *ext = [dictionary[@"thumbnail"] objectForKey:@"extension"];
    entity.thumbnailImageUrl = [NSString stringWithFormat:@"%@/standard_large.%@", path, ext];
    entity.posterImageUrl = [NSString stringWithFormat:@"%@/landscape_incredible.%@", path, ext];
    return entity;
}

@end
