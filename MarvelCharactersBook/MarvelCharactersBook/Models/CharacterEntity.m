//
//  CharacterEntity.m
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "CharacterEntity.h"

@implementation CharacterEntity

-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.characterID forKey:@"characterID"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.characterDescription forKey:@"characterDescription"];
    [encoder encodeObject:self.thumbnailImageUrl forKey:@"thumbnailImageUrl"];
    [encoder encodeObject:self.posterImageUrl forKey:@"posterImageUrl"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.characterID = [decoder decodeObjectForKey:@"characterID"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.characterDescription = [decoder decodeObjectForKey:@"characterDescription"];
        self.thumbnailImageUrl = [decoder decodeObjectForKey:@"thumbnailImageUrl"];
        self.posterImageUrl = [decoder decodeObjectForKey:@"posterImageUrl"];
    }
    return self;
}

@end
