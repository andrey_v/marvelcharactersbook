//
//  CharacterEntity.h
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CharacterEntity : NSObject

@property (nonatomic, strong) NSNumber *characterID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *characterDescription;
@property (nonatomic, strong) NSString *thumbnailImageUrl;
@property (nonatomic, strong) NSString *posterImageUrl;

@end
