//
//  CharacterEntity+Model.h
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "CharacterEntity.h"

@interface CharacterEntity (Model)

+ (NSArray *)entitiesWithArray:(NSArray *)array;

@end
