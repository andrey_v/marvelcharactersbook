//
//  CharacterImageCollectionViewCell.m
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "CharacterImageCollectionViewCell.h"
#import "CharacterEntity.h"
#import "UIImageView+WebCache.h"

@interface CharacterImageCollectionViewCell ()

@property (nonatomic, strong) IBOutlet UIImageView *characterImageView;

@end

@implementation CharacterImageCollectionViewCell

+ (CGFloat)defaultHeight
{
    return 128.0f;
}

- (void)configureWithCharacter:(CharacterEntity *)character
{
    [_characterImageView sd_setImageWithURL:[NSURL URLWithString:character.posterImageUrl]];
}

- (void)dealloc
{
    [_characterImageView release];
    [super dealloc];
}

@end
