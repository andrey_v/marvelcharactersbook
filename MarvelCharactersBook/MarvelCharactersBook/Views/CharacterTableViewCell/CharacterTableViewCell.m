//
//  CharacterTableViewCell.m
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "CharacterTableViewCell.h"
#import "CharacterEntity.h"
#import "UIImageView+WebCache.h"

@interface CharacterTableViewCell ()

@property (nonatomic, strong) IBOutlet UIImageView *characterImageView;
@property (nonatomic, strong) IBOutlet UILabel *characterNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *characterDescriptionLabel;

@property (nonatomic, strong) CharacterEntity *character;

@end

@implementation CharacterTableViewCell

- (void)configureWithCharacter:(CharacterEntity *)character
{
    if (_character.characterID.integerValue != character.characterID.integerValue) {
        _character = character;
        
        [_characterNameLabel setText:character.name];
        [_characterDescriptionLabel setText:character.characterDescription];
        [_characterImageView sd_setImageWithURL:[NSURL URLWithString:character.thumbnailImageUrl]];
    }
}

- (void)dealloc
{
    [_character release];
    [_characterImageView release];
    [_characterNameLabel release];
    [_characterDescriptionLabel release];
    [super dealloc];
}

@end
