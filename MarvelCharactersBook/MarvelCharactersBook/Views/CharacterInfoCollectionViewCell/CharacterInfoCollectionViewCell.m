//
//  CharacterInfoCollectionViewCell.m
//  MarvelCharactersBook
//
//  Created by Andrey Vasilev on 21.08.17.
//  Copyright © 2017 Andrey Vasilev. All rights reserved.
//

#import "CharacterInfoCollectionViewCell.h"
#import "CharacterEntity.h"
#import "NSString+Helpers.h"
#import "AppDelegate.h"

@interface CharacterInfoCollectionViewCell ()

@property (nonatomic, strong) IBOutlet UILabel *characterNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *characterDescriptionLabel;

@end

@implementation CharacterInfoCollectionViewCell

+ (CGFloat)heightForCharacter:(CharacterEntity *)character
{
    return [character.name heightForDefaultWidth] + [character.characterDescription heightForDefaultWidth] + 3*kDefaultSpacing;
}

- (void)configureWithCharacter:(CharacterEntity *)character
{
    [_characterNameLabel setText:character.name];
    [_characterDescriptionLabel setText:character.characterDescription];
}

- (void)dealloc
{
    [_characterNameLabel release];
    [_characterDescriptionLabel release];
    [super dealloc];
}


@end
